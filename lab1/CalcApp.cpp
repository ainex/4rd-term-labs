#include <iostream>
#include <string>
#include <fstream>
#include "ParamsParser.h"
#include "Command.h"
#include "CommandFactory.h"
#include "exception\CommandFactoryException.h"
#include "test\CommandUnitTest.h"

using namespace std;
Command* resolveCommand(int argc, char* argv[]);
void init(string);
int runTests(int argc, char* argv[]);
void logResult(string result, string logFileName);

void main(int argc, char* argv[])

{
	string LOG_FILE_NAME = "operation.log";
	init(LOG_FILE_NAME);

	if (runTests(argc, argv)) 
	{
		return;
	}
	

	try
	{
		Command *command = resolveCommand(argc, argv);
		command->run();
		string result = command->toString();
		cout << result;

		//log result
		logResult(result, LOG_FILE_NAME);	
	}
	catch (exception & e)
	{
		string message = string(e.what());
		cout << e.what() << endl;
	}

	return;
}

Command* resolveCommand(int argc, char* argv[])
{
	Params params = ParamsParser::parse(argc, argv);

	Command* cmd = CommandFactory::getCommand(params);

	return cmd;

}

void init(string logFileName)
{

	ifstream fin;
	fin.open(logFileName);

	if (!fin.is_open())
	{
		//create log file
		ofstream fout;
		fout.open(logFileName);
		fout.close();
	}
	else
	{
		fin.close();

	}

}

int runTests(int argc, char* argv[]) 
{
	string param = argv[1];

	if (argc > 1 && param == "--" + CommandFactory::TEST) {
		CommandUnitTest::runTestSuit();
		return 1;
	}
	else
	{
		return 0;
	}

}

void logResult(string result, string logFileName)
{
	ofstream fout;
	if (result != "") 
	{
		fout.open(logFileName, ios::app);
		fout << result << endl;
		fout.close();
	}
}


