#pragma once
#include <string>

using namespace std;
class Params
{
private:
	bool error;
	string operand1;
	string operand2;
	string command;

public:
	Params::Params();
	bool hasError();
	string getOperand1();
	string getOperand2();
	string getCommand();

	friend ostream& operator<<(ostream& os, const Params& params);
	friend istream& operator>> (istream& is, Params & params);

	void setError(bool error);
	void setOperand1(string operand1);
	void setOperand2(string operand2);
	void setCommand(string operatorSymbol);

};