#pragma once
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
class  Command
{

public:
	static string PREVIOUS_RELUST_ALIAS;
	virtual void run() = 0;
	virtual string toString() = 0;
	virtual ~Command() {  }; 
};