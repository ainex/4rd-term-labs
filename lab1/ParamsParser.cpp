#include "ParamsParser.h"
using namespace std;

string ParamsParser::PARAM_PREFIX = "--";
string ParamsParser::BOOLEAN_MODE = "boolean";
string ParamsParser::ALGEBRAIC_MODE = "algebraic";
string ParamsParser::ERROR = "error";

Params ParamsParser::parse(int argc, char* argv[])
{
	Params params;
	//first arg is exe name, 
	//next, it is expected to have:
	//1) only one command paramp '--help' 
	//2) or at least three params: command and two operands
	if ((argc != 2) && (argc != 4))
	{
		params.setError(true);
		return params;
	}

	string command = argv[1];
	if (string::npos == command.find(PARAM_PREFIX))
	{
		params.setError(true);
		return params;
	};

	command.erase(0, PARAM_PREFIX.length());

	params.setCommand(command);
	if (argc == 4)
	{
		params.setOperand1(argv[2]);
		params.setOperand2(argv[3]);
	}
	return params;

}