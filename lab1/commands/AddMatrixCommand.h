#pragma once
#include "MatrixCommand.h"

using namespace std;

class AddMatrixCommand : public MatrixCommand
{
public:
	void run();
	string toString();
	AddMatrixCommand(int row, int col);
};