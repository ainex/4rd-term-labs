#include <iostream>
#include "SubCommand.h"

using namespace std;

void SubCommand::setOperand1(float op1)
{
	this->operand1 = op1;
}

void SubCommand::setOperand2(float op2)
{
	this -> operand2 = op2;
}

float SubCommand::getResult()
{
	return this->result;
}

void SubCommand::run()
{
	this->result = operand1 - operand2;
	
}

string SubCommand::toString()
{
	return "Substract command: "+ to_string(operand1) + " - " + to_string(operand2) + " = " + to_string(result);
}