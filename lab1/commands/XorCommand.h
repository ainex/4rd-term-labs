#pragma once
#include "BooleanCommand.h"

using namespace std;

class XorCommand : public BooleanCommand
{

public:
	~XorCommand() {};
	void run();
	string toString();
};
