#pragma once
#include "../Command.h"

using namespace std;

class BooleanCommand : public Command
{
protected:
	int operand1;
	int operand2;
	int result;


public:
	virtual ~BooleanCommand() {};
	void setOperand1(int op1);
	void setOperand2(int op2);
	int getResult();

};
