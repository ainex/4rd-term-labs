#include "DivideCommand.h"
using namespace std;


void DivideCommand::setOperand1(float op1)
{
	this->operand1 = op1;
}

void DivideCommand::setOperand2(float op2)
{
	this->operand2 = op2;
}


float DivideCommand::getResult()
{
	return this->result;
}

void DivideCommand::run()
{
	this->result = operand1 / operand2;
}

string DivideCommand::toString()
{
	return "Divide Command: "  + to_string(operand1) + " / " + to_string(operand2) + " = " + to_string(result);
}


