#include "AddCommand.h"

using namespace std;
string Command::PREVIOUS_RELUST_ALIAS = "$result";



void AddCommand::setOperand1(float op1)
{
	this->operand1 = op1;
}

void AddCommand::setOperand2(float op2)
{
	this -> operand2 = op2;
}

float AddCommand::getResult()
{
	return this->result;
}

void AddCommand::run()
{
	this->result = operand1 + operand2;
	saveResult();
}

void AddCommand::saveResult()
{
	ofstream fout("float.dat", ios::binary | ios::out);
	if (fout) {
		fout.write((char*)&(this->result), sizeof (this->result)); 
		fout.close();
	}
}

string AddCommand::toString()
{
	return  "Add Command: " + to_string(operand1) + " + " + to_string(operand2) + " = " + to_string(result);
}