#pragma once
#include "../Command.h"

using namespace std;

class DivideCommand : public Command
{
private:
	float operand1;
	float operand2;
	float result;


public:
	void setOperand1(float op1);
	void setOperand2(float op2);
	float getResult();
	void run();
	string toString();
};