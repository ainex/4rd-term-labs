#include "LogCommand.h"

using namespace std;

string LogCommand::FILE_NAME = "operation.log";

void LogCommand::run()
{

	ifstream filein(FILE_NAME);

	if (filein.is_open())

	{	
		vector<string> logLines;
		for (string line; getline(filein, line); )
		{
			logLines.push_back(line);
		}

		for (vector<string>::iterator it = logLines.begin(); it != logLines.end(); ++it)
		{
			cout << *it << endl;
		}

	}
}

string LogCommand::toString()
{
	return "";
}
