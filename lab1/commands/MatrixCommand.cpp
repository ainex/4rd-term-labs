#include "MatrixCommand.h"

int MatrixCommand::MAX_SIZE = 10;

MatrixCommand::MatrixCommand(int row, int col)
{
	if (row > 0 && row <= MAX_SIZE && col > 0 && col <= MAX_SIZE)
	{
		this->row = row;
		this->col = col;
		this -> operand1 = new double*[row];
		this-> operand2 = new double*[row];
		this-> result = new double*[row];
		for (int i = 0; i < row; i++) {
			operand1[i] = NULL;
			operand2[i] = NULL;
			result[i] = NULL;
		}
	}
}

MatrixCommand::~MatrixCommand()
{
	destruct(operand1);
	destruct(operand2);
	destruct(result);
}
void MatrixCommand::setOperand1(double ** op1)
{
	this->operand1 = op1;
}
void MatrixCommand::setOperand2(double ** op2)
{
	this->operand2 = op2;
};

double ** MatrixCommand::getResult()
{
	return this->result;
}

void MatrixCommand::printResult()
{
	printMatrix(this->result);
}

void MatrixCommand::initOperands()
{
	cout << "Initializing two matrix operands, dimensions " << row << "X" << col << endl;
	initMatrix(operand1);
	initMatrix(operand2);
}

void MatrixCommand::initMatrix(double ** matrix)
{
	cout << "enter a " << row << "x" << col << " matix, one row in a line:" << endl;
	double* p = NULL;
	double element;
	for (int i = 0; i < row; i++) {
		p = new double[col];
		matrix[i] = p;
		for (int j = 0; j < col; j++) {
			cin >> element;
			matrix[i][j] = element;
		}

	}
}

void MatrixCommand::printMatrix(double ** matrix)
{
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
}

void MatrixCommand::destruct(double ** matrix)
{
	if (NULL != matrix)
	{
		for (int i = 0; i < row; i++) {
			if (NULL != matrix[i]) delete[] matrix[i];
		}
		delete[] matrix;
	}
};
