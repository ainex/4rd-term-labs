#include "SubMatrixCommand.h"

void SubMatrixCommand::run()
{
	double* p = NULL;
	for (int i = 0; i < row; i++) {
		p = new double[col];
		this->result[i] = p;
		for (int j = 0; j < col; j++) {
			this->result[i][j] = this->operand1[i][j] - this->operand2[i][j];
		}
	}
	cout << "Sub Matrix Command: " << endl;
	printMatrix(operand1);
	cout << "substract matrix" << endl;
	printMatrix(operand2);
	cout << "result matrix" << endl;
	printMatrix(result);

}

SubMatrixCommand::SubMatrixCommand(int row, int col) :MatrixCommand(row, col)
{}

string SubMatrixCommand::toString()
{
	return "Sub Matrix Command: " + to_string(1) + "substract matrix" + to_string(2) + "result matrix" + to_string(3);
}
