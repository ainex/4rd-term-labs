#include "XorCommand.h"

void XorCommand::run()
{
	this->result = operand1 ^ operand2;
	
}
string XorCommand::toString()
{
	return "Xor Command: " + to_string(operand1) + " XOR " + to_string(operand2) + " = " + to_string(result);
}
