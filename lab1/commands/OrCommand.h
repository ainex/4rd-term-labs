#pragma once
#include "BooleanCommand.h"

using namespace std;

class OrCommand : public BooleanCommand
{

public:
	~OrCommand() {};
	void run();
	string toString();
};
