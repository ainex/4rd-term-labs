#include <iostream>
#include "HeplCommand.h"

using namespace std;

void HelpCommand::run()
{
	cout << "to calculate" << endl;
	cout << "type [--command_name] [operand_1] [operand_2]" << endl;
	cout << "example:" <<endl << "--add 25 5" << endl;
	cout << "commands: " << endl;
	cout << "--add : Addition" << endl;
	cout << "--sub : Subtraction" << endl;
	cout << "--mult : Multiplication" << endl;
	cout << "--div : Division" << endl;
	cout << "--OR : Addition boolean" << endl;
	cout << "--XOR : Subtraction boolean" << endl;
	cout << "--add_matrix [rows] [columns]: Addition for matrix" << endl;
	cout << "--sub_matrix [rows] [columns]: Subtraction for matrix" << endl;
	cout << "--log : Print operation log" << endl;
	cout << "type [$result] instead of [operand_1] or [operand_2] to use previous result" << endl;
	cout << "to repeat help message" << endl;
	cout << "type --help " << endl;
}

string HelpCommand::toString()
{
	return "";
}
