#include "MultiplyCommand.h"

using namespace std;

void MultiplyCommand::setOperand1(float op1)
{
	this->operand1 = op1;
}


void MultiplyCommand::setOperand2(float op2)
{
	this->operand2 = op2;
}

float MultiplyCommand::getResult()
{
	return this->result;
}

void MultiplyCommand::run()
{
	this->result = operand1 * operand2;
}

string MultiplyCommand::toString()
{
	return  "Multiply Command: " + to_string(operand1) + " * " + to_string(operand2) + " = " + to_string(result);
}
