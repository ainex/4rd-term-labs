#include "AddMatrixCommand.h"

void AddMatrixCommand::run()
{
	double* p = NULL;
	for (int i = 0; i < row; i++) {
		p = new double[col];
		this->result[i] = p;
		for (int j = 0; j < col; j++) {
			this->result[i][j] = this->operand1[i][j] + this->operand2[i][j];
		}
	}
	cout << "Add Matrix Command: " << endl;
	printMatrix(operand1);
	cout << "add matrix" << endl;
	printMatrix(operand2);
	cout << "result matrix" << endl;
	printMatrix(result);
	
}

AddMatrixCommand::AddMatrixCommand(int row, int col) :MatrixCommand(row, col)
{

}

string AddMatrixCommand::toString()
{
	return "Add Matrix Command: " + to_string(1) + "add matrix" + to_string(2) + "result matrix" + to_string(3);
}
