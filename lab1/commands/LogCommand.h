#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "../Command.h"

using namespace std;

class LogCommand : public Command
{
private:
	static string FILE_NAME;

public:
	void run();
	string toString();
};
