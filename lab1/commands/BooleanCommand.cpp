#include "BooleanCommand.h"

void BooleanCommand::setOperand1(int op1)
{
	this->operand1 = op1;
}

void BooleanCommand::setOperand2(int op2)
{
	this->operand2 = op2;
}

int BooleanCommand::getResult()
{
	return this->result;
}
