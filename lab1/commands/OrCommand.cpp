#include "OrCommand.h"



void OrCommand::run()
{
	this->result = operand1 | operand2;
}

string OrCommand::toString()
{
	return "Or Command: " + to_string(operand1) + " OR " + to_string(operand2) + " = " + to_string(result);
}
