#pragma once
#include "../Command.h"

using namespace std;

class HelpCommand : public Command
{

public:
	void run();
	string toString();
};

