#pragma once
#include "MatrixCommand.h"

using namespace std;

class SubMatrixCommand : public MatrixCommand
{
public:
	void run();
	string toString();
	SubMatrixCommand(int row, int col);
};