#pragma once
#include "../Command.h"

using namespace std;

class MatrixCommand : public Command
{
public:
	static int MAX_SIZE;
	MatrixCommand(int row, int col);
	~MatrixCommand();
	void setOperand1(double ** op1);
	void setOperand2(double ** op2);
	double **getResult();
	void printResult();
	void initOperands();

protected:
	int row;
	int col;
	double **operand1 = NULL;
	double **operand2 = NULL;
	double **result = NULL;
	void printMatrix(double **matrix);

private:
	void initMatrix(double ** matrix);
	void destruct(double ** matrix);
	

};
//// Now allocate memory for columns
//for (int i = 0; i < COL; i++) {
//	pvalue[i] = new double[COL];
//}
