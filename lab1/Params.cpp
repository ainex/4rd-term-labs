#include "Params.h"


Params::Params()
{
	this->error = false;
}

bool Params::hasError()
{
	return this->error;
}

string Params::getOperand1()
{
	return this->operand1;
}

string Params::getOperand2()
{
	return this->operand2;
}

string Params::getCommand()
{
	return this->command;
}

void Params::setError(bool error)
{
	this->error = error;
}

void Params::setOperand1(string operand1)
{
	this->operand1 = operand1;
}

void Params::setOperand2(string operand2)
{
	this->operand2 = operand2;
}

void Params::setCommand(string command)
{
	this->command = command;
}

ostream & operator<<(ostream & os, const Params & params)
{
	os << "{ " <<"command:" << params.command << ", operand1:" << params.operand1 << ", operand2:" << params.operand2   << " }" << endl;
	return os;
}

istream& operator>> (istream& is, Params & params)
{
	is >> params.command >> params.operand1 >> params.operand2;
	return is;
}
