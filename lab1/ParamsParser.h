#pragma once
#include <assert.h>
#include "Params.h"

class ParamsParser
{
private:
	static string PARAM_PREFIX;
	static string BOOLEAN_MODE;
	static string ALGEBRAIC_MODE;
	static string ERROR;

public:
	static Params parse(int argc, char* argv[]);
};