#pragma once
#include <string>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <sstream>
#include <typeinfo>

class CalcException : public std::runtime_error
{
	
public:
	CalcException(const std::string aMessage, const char* fileName, const std::size_t lineNumber);
		
private:
	const char* fileName;
	std::size_t lineNumber;
	std::string message;
};
