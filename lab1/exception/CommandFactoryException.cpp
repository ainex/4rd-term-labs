#include "CommandFactoryException.h"

CommandFactoryException::CommandFactoryException(const std::string aMessage, const char * fileName, const std::size_t lineNumber)
	: CalcException(aMessage, fileName, lineNumber)
{
}
