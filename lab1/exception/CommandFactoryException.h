#pragma once
#include "CalcException.h"


class CommandFactoryException : public CalcException
{

public:
	CommandFactoryException(const std::string aMessage, const char* fileName, const std::size_t lineNumber);

};
