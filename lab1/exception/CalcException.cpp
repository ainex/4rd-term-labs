#include "CalcException.h"

CalcException::CalcException(const std::string aMessage, const char * fileName, const std::size_t lineNumber): std::runtime_error("")
{
	this->fileName = fileName;
	this->lineNumber = lineNumber;
	this->message = aMessage;

	std::stringstream ss;
	ss << "[ERROR] \t" << typeid(*this).name() << ": " << message << ". File: "  <<fileName <<  " on line: " << lineNumber;
	static_cast<std::runtime_error&>(*this) = std::runtime_error(ss.str());
}

