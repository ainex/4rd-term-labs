#pragma once
#include "Command.h"
#include "commands/AddCommand.h"
#include "commands/SubCommand.h"
#include "commands/MultiplyCommand.h"
#include "commands/DivideCommand.h"
#include "commands/HeplCommand.h"
#include "commands/OrCommand.h"
#include "commands/XorCommand.h"
#include "commands/AddMatrixCommand.h"
#include "commands/SubMatrixCommand.h"
#include "commands/LogCommand.h"
#include "Params.h"
#include "exception\CommandFactoryException.h"

class CommandFactory
{
public:
	static string ADD; //Addition
	static string SUB; //Subtraction
	static string MULT;//Multiplication
	static string DIV; //Division
	static string OR; //Addition boolean
	static string XOR; //Subtraction boolean
	static string ADD_MATRIX; //Addition for matrix
	static string SUB_MATRIX; //Subtraction for matrix
	static string LOG; //operation log
	static string TEST; //test

	static string FLOAT_RESULT_DATA;

	static Command* getCommand(Params params);

private:
	static float stringToFloat(string strFloat);
	static int stringToInt(string strInt);
	static float readPreviousResult();
};
