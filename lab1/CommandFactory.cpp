#include <iostream>
#include "CommandFactory.h"


using namespace std;
string CommandFactory::ADD = "add"; //Addition
string CommandFactory::SUB = "sub"; //Subtraction
string CommandFactory::MULT = "mult";//Multiplication
string CommandFactory::DIV = "div"; //Division
string CommandFactory::OR = "OR"; //Addition boolean
string CommandFactory::XOR = "XOR"; //Subtraction boolean
string CommandFactory::ADD_MATRIX = "add_matrix"; //Addition for matrix
string CommandFactory::SUB_MATRIX = "sub_matrix"; //Subtraction for matrix
string CommandFactory::LOG = "log"; //Operation log
string CommandFactory::TEST = "test"; //test

string CommandFactory::FLOAT_RESULT_DATA = "float.dat";


Command * CommandFactory::getCommand(Params params)
{
	if (params.hasError())
	{
		return new HelpCommand();
	}

	string cmd = params.getCommand();
	if (ADD == cmd)
	{
		AddCommand *addCommand = new AddCommand();
		if (params.getOperand1() == Command::PREVIOUS_RELUST_ALIAS) {
			addCommand->setOperand1(readPreviousResult());
		}
		else 
		{
			addCommand->setOperand1(stringToFloat(params.getOperand1()));
		}
		if (params.getOperand2() == Command::PREVIOUS_RELUST_ALIAS) {
			addCommand->setOperand2(readPreviousResult());
		}
		else
		{
			addCommand->setOperand2(stringToFloat(params.getOperand2()));
		}
				
		return addCommand;
	}
	else if (SUB == cmd)
	{
		SubCommand *subCommand = new SubCommand();
		subCommand->setOperand1(stringToFloat(params.getOperand1()));
		subCommand->setOperand2(stringToFloat(params.getOperand2()));
		return subCommand;
	}
	else if (MULT == cmd)
	{
		MultiplyCommand *multCommand = new MultiplyCommand();
		multCommand->setOperand1(stringToFloat(params.getOperand1()));
		multCommand->setOperand2(stringToFloat(params.getOperand2()));
		return multCommand;
	}
	else if (DIV == cmd)
	{
		DivideCommand *divCommand = new DivideCommand();
		divCommand->setOperand1(stringToFloat(params.getOperand1()));
		divCommand->setOperand2(stringToFloat(params.getOperand2()));
		return divCommand;
	}
	else if ((OR == cmd) || (XOR == cmd))
	{
		BooleanCommand *booleanCommand = NULL;
		if (OR == cmd)
		{
			booleanCommand = new OrCommand();
		}
		else 
		{
			booleanCommand = new XorCommand();
		}
		booleanCommand->setOperand1(stringToInt(params.getOperand1()));
		booleanCommand->setOperand2(stringToInt(params.getOperand2()));
		return booleanCommand;
	}
	else if ((ADD_MATRIX == cmd) || (SUB_MATRIX == cmd))
	{
		MatrixCommand *matrixCommand = NULL;
		if (ADD_MATRIX == cmd)
		{
			matrixCommand = new AddMatrixCommand(stringToInt(params.getOperand1()), stringToInt(params.getOperand2()));
		}
		else
		{
			matrixCommand = new SubMatrixCommand(stringToInt(params.getOperand1()), stringToInt(params.getOperand2()));
		}
		matrixCommand->initOperands();
		return matrixCommand;

	}
	else if (LOG == cmd)
	{
		return new LogCommand();
	}

	else return new HelpCommand();


}

float CommandFactory::stringToFloat(string strFloat)
{
	try
	{
		float number = std::stof(strFloat);

		return number;
	}
	catch (exception & e)
	{
		throw CommandFactoryException("failed to cast string to float while parsing command", __FILE__, __LINE__);
	}
}

int CommandFactory::stringToInt(string strInt)
{

	int number = std::stoi(strInt);

	return number;
}

float CommandFactory::readPreviousResult()
{
	float previousResult;
	ifstream in("float.dat", ios::binary | ios::in); 
	in.read((char*)&previousResult, sizeof previousResult);
	in.close(); 
	return previousResult;
}
