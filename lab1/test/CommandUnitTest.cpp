#include "CommandUnitTest.h"

void CommandUnitTest::runTestSuit()
{
	cout << "Running CommandUnitTest::runTestSuit()" << endl;
	/*testAddCommand();
	testSubtractCommand();
	testMultiplyCommand();
	testDivideCommand();

	testXORCommand();
	testORCommand();*/

	// testAddMatrixCommand();
	//testSubMatrixCommand();

	//testAddCommandWithPreviousResult();
	//testOutstreamOverload();
	//testIstreamOverload();

	testCommandFactoryException();
}

void CommandUnitTest::testAddCommand()
{
	AddCommand *command = new AddCommand();
	cout << "testAddCommand... ";
	command->setOperand1(25);
	command->setOperand2(10);
	command->run();
	float result = command->getResult();
	bool passed = assertResult(35, result);
	printResult(passed);
}

void CommandUnitTest::testSubtractCommand()
{
	SubCommand  *command = new SubCommand();
	cout << "testSubtractCommand... ";
	command->setOperand1(15.2);
	command->setOperand2(39);
	command->run();
	float result = command->getResult();
	bool passed = assertResult(15.2-39, result);
	printResult(passed);
}

void CommandUnitTest::testMultiplyCommand()
{
	MultiplyCommand  *command = new MultiplyCommand();
	cout << "testMultiplyCommand... ";
	command->setOperand1(15);
	command->setOperand2(0.5);
	command->run();
	float result = command->getResult();
	bool passed = assertResult(15*0.5, result);
	printResult(passed);
}

void CommandUnitTest::testDivideCommand()
{
	DivideCommand  *command = new DivideCommand();
	cout << "testDivideCommand... ";
	command->setOperand1(24);
	command->setOperand2(12);
	command->run();
	float result = command->getResult();
	bool passed = assertResult(24/12.0, result);
	printResult(passed);
}

void CommandUnitTest::testXORCommand()
{
	XorCommand  *command = new XorCommand();
	cout << "testXORCommand... ";
	command->setOperand1(1);
	command->setOperand2(0);
	command->run();
	int result = command->getResult();
	bool passed = (result == (1 ^ 0));
	printResult(passed);

	command->setOperand1(0);
	command->setOperand2(1);
	command->run();
	result = command->getResult();
	passed = (result == (0 ^ 1));
	printResult(passed);

	command->setOperand1(1);
	command->setOperand2(1);
	command->run();
	result = command->getResult();
	passed = (result == (1 ^ 1));
	printResult(passed);
}

void CommandUnitTest::testORCommand()
{
	OrCommand  *command = new OrCommand();
	cout << "testORCommand... ";
	command->setOperand1(1);
	command->setOperand2(0);
	command->run();
	int result = command->getResult();
	bool passed = (result == (1 | 0));
	printResult(passed);

	command->setOperand1(0);
	command->setOperand2(0);
	command->run();
	result = command->getResult();
	passed = (result == (0 | 0));
	printResult(passed);
}

void CommandUnitTest::testAddMatrixCommand()
{
	cout << "testAddMatrixCommand... ";
	int row = 2;
	int col = 2;
	AddMatrixCommand *addMatrixCommand = new AddMatrixCommand(row, col);
	double **operand1 =  new double*[row];
	double **operand2 = new double*[row];
	double **expectedMatrix = new double*[row];
	fillMatrixWith(operand1, row, col, 1);
	fillMatrixWith(operand2, row, col, 2);
	fillMatrixWith(expectedMatrix, row, col, 3);
	addMatrixCommand->setOperand1(operand1);
	addMatrixCommand->setOperand2(operand2);
	addMatrixCommand->run();
	
	double ** resultMatrix =  addMatrixCommand->getResult();
	
	bool matrixEquals = isMatrixEquals(expectedMatrix, resultMatrix, row, col);
	printResult(matrixEquals);

}

void CommandUnitTest::testSubMatrixCommand()
{
	cout << "testSubMatrixCommand... ";
	int row = 2;
	int col = 2;
	SubMatrixCommand *subMatrixCommand = new SubMatrixCommand(row, col);
	double **operand1 = new double*[row];
	double **operand2 = new double*[row];
	double **expectedMatrix = new double*[row];
	fillMatrixWith(operand1, row, col, 5);
	fillMatrixWith(operand2, row, col, 2);
	fillMatrixWith(expectedMatrix, row, col, 3);
	subMatrixCommand->setOperand1(operand1);
	subMatrixCommand->setOperand2(operand2);
	subMatrixCommand->run();

	double ** resultMatrix = subMatrixCommand->getResult();
	bool matrixEquals = isMatrixEquals(expectedMatrix, resultMatrix, row, col);
	printResult(matrixEquals);
	
}

void CommandUnitTest::testAddCommandWithPreviousResult()
{
	AddCommand *command = new AddCommand();
	cout << "testAddCommand with previous result... ";
	command->setOperand1(25);
	command->setOperand2(10);
	command->run();
	float result = command->getResult();
	cout << endl << "previous result: " << result << endl;
	cout << "test command: --add $result 25" << endl;

	Params params;
	params.setOperand1("$result");
	params.setOperand2("25");
	params.setCommand("add");

	Command* cmd = CommandFactory::getCommand(params);
	cmd->run();
	cout << cmd->toString() << endl;

}

void CommandUnitTest::testOutstreamOverload()
{
	cout << "testOutstreamOverload ..." << endl;
	Params p;
	p.setCommand("testCommand");
	p.setOperand1("testOperand1");
	p.setOperand2("testOperand2");
	cout << p;
	printResult(true);
}

void CommandUnitTest::testIstreamOverload()
{
	cout << "testIstreamOverload ..." << endl;
	string testDataFileName = "test_isteam_overload.txt";
	cout << "reading from file:  "<< testDataFileName  << endl;

	Params p;
	ifstream fin(testDataFileName);
	if (fin.is_open())
	{
		fin >> p; 
		fin.close();
		cout << p;
		printResult(true);
	}
	
	else {
		std::cout << "Error opening file";
		printResult(false);
	}
	
	
}

void CommandUnitTest::testCommandFactoryException()
{
	cout << "testCommandFactoryException ..." << endl;
	Params params;
	params.setOperand1("err0r");
	params.setOperand2("25");
	params.setCommand("add");
	try
	{
		CommandFactory::getCommand(params);
		printResult(false);
	}
	catch (exception& e) 
	{
		cout << "got exception" << endl;
		cout << e.what() << endl;
		printResult(true);
	}
}



bool CommandUnitTest::assertResult(float expected, float fact)
{
	
	return expected == fact;
}

void CommandUnitTest::printResult(bool result)
{
	if (result)
	{
		cout << "passed" << endl << endl;
	}
	else
	{
		cout << "failed" << endl << endl;
	}
}

void CommandUnitTest::fillMatrixWith(double ** matrix, int row, int col, double value)
{
	for (int i = 0; i < row; i++) {
		matrix[i] = new double[col];
		for (int j = 0; j < col; j++) {
			matrix[i][j] = value;
		}
	}
}

bool CommandUnitTest::isMatrixEquals(double ** matrixA, double ** matrixB, int row, int col, double epsilon)
{
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (std::abs(matrixA[i][j] - matrixB[i][j]) > epsilon)
			{
				return false;
			}
		}
	}
	return true;
}

