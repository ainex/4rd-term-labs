#pragma once
#include <iostream>
#include <string>
#include "../ParamsParser.h"
#include "../Command.h"
#include "../CommandFactory.h"
#include "../commands/AddCommand.h"
#include "../commands/SubCommand.h"
#include "../commands/MultiplyCommand.h"
#include "../commands/DivideCommand.h"
#include "../commands/HeplCommand.h"
#include "../commands/OrCommand.h"
#include "../commands/XorCommand.h"
#include "../commands/AddMatrixCommand.h"
#include "../commands/SubMatrixCommand.h"

class CommandUnitTest 
{
public:
	static void runTestSuit();
	static void testAddCommand();
	static void testSubtractCommand();
	static void testMultiplyCommand();
	static void testDivideCommand();
	static void testORCommand();
	static void testXORCommand();
	static void testAddMatrixCommand();
	static void testSubMatrixCommand();

	static void testAddCommandWithPreviousResult();


	static bool assertResult(float expected, float fact);
	static void printResult(bool result);
	static void fillMatrixWith(double **matrix, int row, int col, double value);
	//todo move to matrix class
	static bool isMatrixEquals(double **matrixA, double **matrixB, int row, int col, double epsilon = 0.001);

	static void testOutstreamOverload();
	static void testIstreamOverload();

	static void testCommandFactoryException();
};